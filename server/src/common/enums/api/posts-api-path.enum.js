const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  REACTIONS: '/reactions'
};

export { PostsApiPath };
