import React, { useEffect } from 'react';
import { SafeAreaView, Text, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { CompanyLogo, AuthForm, FramedText } from '../../components';
import { Screens, userState } from '../../common/enums';
import { logOut, signUp } from '../../store/actions';
import styles from './styles';

const SignUpView = ({ navigation }) => {
  const dispatch = useDispatch();
  const { id } = useSelector(state => state.user);

  useEffect(() => {
    if (id !== userState.UNAUTHORIZED) {
      navigation.navigate(Screens.HOME);
    }
  }, [id]);

  const navigate = () => {
    dispatch(logOut());
    navigation.goBack();
  };

  const onConfirm = (email, name, password) => {
    if (!email || !password || !name) {
      Alert.alert('All fields are required');
    } else dispatch(signUp({ email, name, password }));
  };

  return (
    <SafeAreaView style={styles.container}>
      <CompanyLogo name="Thread" logoUri="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png" />
      <Text style={styles.label}>Log in to your account</Text>
      <AuthForm isSignUp buttonText="Register" onConfirm={onConfirm} />
      <FramedText label="Alredy with us?" link="Log In" navigate={navigate} />
    </SafeAreaView>
  );
};

export { SignUpView };
