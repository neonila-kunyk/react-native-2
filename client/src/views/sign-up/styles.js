import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.MOST_LIGHT_GREY,
    padding: '2rem',
    justifyContent: 'center',
    alignItems: 'center'
  },
  label: {
    fontSize: '1.5rem',
    fontWeight: '700',
    color: Colors.GREEN,
    marginTop: '0.75rem'
  }
});
