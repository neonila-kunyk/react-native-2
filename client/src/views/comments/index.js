import React, { useState, useRef, useEffect } from 'react';
import { SafeAreaView, ScrollView, View, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { CreatePostForm, Post, CopyModal, Exit, Comment } from '../../components';
import { createComment, likePost, toggleExpandedPost } from '../../store/actions';
import { EmptyView } from '../empty';
import styles from './styles';

const CommentsView = ({ navigation }) => {
  const dispatch = useDispatch();
  const { posts, expandedPost, likedPosts, comments, errorMessage } = useSelector(state => state.posts);
  const [sharedPostId, setSharedPostId] = useState('');
  const scrollViewRef = useRef();

  useEffect(() => {
    if (errorMessage) {
      Alert.alert(errorMessage);
    }
  }, [errorMessage]);

  const onExit = () => {
    navigation.goBack();
    dispatch(toggleExpandedPost());
  };

  if (expandedPost) {
    return (
      <SafeAreaView style={styles.container}>
        <Exit onPress={onExit} />
        <ScrollView ref={scrollViewRef}>
          <Post
            style={styles.post}
            post={posts.find(post => post.id === expandedPost)}
            onPostLike={id => dispatch(likePost(id))}
            sharePost={id => setSharedPostId(id)}
            isLiked={likedPosts.includes(expandedPost)}
          />
          <View style={styles.comments}>
            {comments.map(comment => <Comment comment={comment} key={comment.id} />)}
          </View>
          <CreatePostForm
            style={styles.form}
            height="8.5rem"
            numberOfRows={5}
            placeholder="Type a comment..."
            secondButton={{
              text: 'Post comment',
              onPress: ({ body }) => { dispatch(createComment({ body, postId: expandedPost })); }
            }}
          />
        </ScrollView>
        <CopyModal
          title="Share Post"
          text={sharedPostId}
          isModalVisible={!!sharedPostId}
          hide={() => { setSharedPostId(''); }}
        />
      </SafeAreaView>
    );
  }
  return <EmptyView />;
};

export { CommentsView };
