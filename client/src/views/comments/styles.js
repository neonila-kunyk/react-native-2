import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.WHITE,
    paddingTop: '2rem',
    justifyContent: 'center'
  },
  post: {
    width: '100% - 1.5rem',
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    marginHorizontal: '0.75rem',
    borderWidth: 1,
    borderColor: Colors.LIGHT_GREY,
    borderRadius: 5
  },
  comments: {
    flex: 1
  },
  form: {
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0
  }
});
