import React from 'react';
import { SafeAreaView, Image } from 'react-native';
import { useSelector } from 'react-redux';
import { DisabledInput } from '../../components';
import { DEFAULT_USER_AVATAR } from '../../common/constants';
import styles from './styles';

const ProfileView = () => {
  const { email, name, image } = useSelector(state => state.user);

  return (
    <SafeAreaView style={styles.container}>
      <Image
        style={styles.image}
        source={{ uri: image?.link ?? DEFAULT_USER_AVATAR }}
      />
      <DisabledInput
        icon="user"
        isEditable={false}
        value={name}
      />
      <DisabledInput
        icon="at"
        isEditable={false}
        value={email}
      />
    </SafeAreaView>
  );
};

export { ProfileView };
