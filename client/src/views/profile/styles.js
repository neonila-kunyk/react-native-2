import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: Colors.MORE_LIGHT_GREY,
    padding: '4rem',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    height: '18rem',
    width: '18rem',
    borderRadius: 1000,
    marginBottom: '3rem'
  }
});
