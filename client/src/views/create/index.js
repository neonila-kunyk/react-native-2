import React, { useEffect } from 'react';
import { SafeAreaView, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { CreatePostForm } from '../../components';
import { imageService } from '../../services';
import { createPost } from '../../store/actions';
import styles from './styles';

const CreateView = () => {
  const dispatch = useDispatch();
  const { errorMessage } = useSelector(state => state.posts);

  useEffect(() => {
    if (errorMessage) {
      Alert.alert(errorMessage);
    }
  }, [errorMessage]);

  return (
    <SafeAreaView style={styles.container}>
      <CreatePostForm
        placeholder="What is the news?"
        autoFocus
        firstButton={{
          icon: 'image',
          text: 'Attach image',
          onPress: imageUri => imageService.uploadImage(imageUri)
        }}
        secondButton={{
          text: 'Post',
          onPress: post => { dispatch(createPost(post)); }
        }}
      />
    </SafeAreaView>
  );
};

export { CreateView };
