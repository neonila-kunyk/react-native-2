import React, { useEffect } from 'react';
import { SafeAreaView, Text, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { CompanyLogo, AuthForm, FramedText } from '../../components';
import { Screens, userState } from '../../common/enums';
import { logOut, logIn } from '../../store/actions';
import styles from './styles';

const LogInView = ({ navigation }) => {
  const dispatch = useDispatch();
  const { id } = useSelector(state => state.user);

  useEffect(() => {
    if (id === userState.UNREGISTRATED) {
      Alert.alert('Invalid email or password');
    } else if (id !== userState.UNAUTHORIZED) {
      navigation.navigate(Screens.HOME);
    }
  }, [id]);

  const navigate = () => {
    dispatch(logOut());
    navigation.navigate(Screens.SIGN_UP);
  };

  const onConfirm = (email, password) => {
    if (!email || !password) {
      Alert.alert('All fields are required');
    } else dispatch(logIn({ email, password }));
  };

  return (
    <SafeAreaView style={styles.container}>
      <CompanyLogo name="Thread" logoUri="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png" />
      <Text style={styles.label}>Log in to your account</Text>
      <AuthForm buttonText="Log in" onConfirm={onConfirm} />
      <FramedText label="New to us?" link="Sign Up" navigate={navigate} />
    </SafeAreaView>
  );
};

export { LogInView };
