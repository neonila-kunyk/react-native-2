import React, { useEffect, useState } from 'react';
import { SafeAreaView, Alert, ActivityIndicator } from 'react-native';
import { FlatList } from 'react-native-bidirectional-infinite-scroll';
import { useDispatch, useSelector } from 'react-redux';
import {
  loadLikedPosts,
  loadPosts,
  resetPosts,
  toggleExpandedPost,
  likePost
} from '../../store/actions';
import { ToggleFilter } from '../../components/toggle-filter';
import { LIMIT } from '../../common/constants';
import { Filter, Colors, Screens } from '../../common/enums';
import styles from './styles';
import { CopyModal, Post } from '../../components';

const PostsView = ({ navigation }) => {
  const dispatch = useDispatch();
  const { posts, isAllPosts, userId, likedPosts, errorMessage } = useSelector(state => ({
    posts: state.posts.posts,
    isAllPosts: state.posts.isAllPosts,
    userId: state.user.id,
    likedPosts: state.posts.likedPosts
  }));
  const [sharedPostId, setSharedPostId] = useState('');
  const [filter, setFilter] = useState(Filter.NONE);
  const [isFilterVisible, setIsFilterVisible] = useState(false);

  useEffect(() => {
    if (errorMessage) {
      Alert.alert(errorMessage);
    }
  }, [errorMessage]);

  const getLoadFilter = (id = undefined) => ({ count: LIMIT, from: posts.length, userId: id });

  const loadMorePosts = () => {
    if (!isAllPosts) {
      if (filter === Filter.FIRST) {
        dispatch(loadPosts(getLoadFilter(userId)));
      } else {
        dispatch(loadPosts(getLoadFilter()));
      }
    }
  };

  const loadFilteredPosts = (id = undefined) => dispatch(resetPosts({ ...getLoadFilter(id), from: 0 }));

  useEffect(() => {
    dispatch(loadLikedPosts());
    dispatch(loadPosts(getLoadFilter()));
  }, []);

  const openComments = id => {
    dispatch(toggleExpandedPost(id));
    navigation.navigate(Screens.COMMENTS);
  };

  const renderPost = ({ item }) => (
    <Post
      post={item}
      onPostLike={id => dispatch(likePost(id))}
      sharePost={id => setSharedPostId(id)}
      openComments={openComments}
      isLiked={likedPosts.includes(item.id)}
    />
  );

  return !posts.length
    ? (
      <SafeAreaView style={styles.container}>
        <ActivityIndicator size="large" />
      </SafeAreaView>
    )
    : (
      <SafeAreaView style={styles.container}>
        <ToggleFilter
          firstSwitch={{
            label: 'Show only my posts',
            onToggle: () => loadFilteredPosts(userId)
          }}
          secondSwitch={{
            label: 'Show only others posts',
            onToggle: loadFilteredPosts
          }}
          onOff={loadFilteredPosts}
          filter={filter}
          setFilter={setFilter}
          isFilterVisible={isFilterVisible}
          setIsFilterVisible={setIsFilterVisible}
        />
        <FlatList
          data={filter === Filter.SECOND ? posts.filter(post => post.userId !== userId) : posts}
          renderItem={renderPost}
          keyExtractor={item => item.id}
          onStartReached={async () => {}}
          onEndReached={async () => loadMorePosts()}
          showDefaultLoadingIndicators
          onEndReachedThreshold={20}
          activityIndicatorColor={Colors.BLUE}
        />
        <CopyModal
          title="Share Post"
          text={sharedPostId}
          isModalVisible={!!sharedPostId}
          hide={() => { setSharedPostId(''); }}
        />
      </SafeAreaView>
    );
};

export { PostsView };
