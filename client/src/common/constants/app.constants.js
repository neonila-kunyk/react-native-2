const DEFAULT_LOGO = 'https://www.freepnglogos.com/uploads/logo-3d-png/3d-company-logos-design-logo-online-2.png';
const IMAGE_CONTAINER = 'https://rlv.zcache.ca/create_your_own_photo_print-r7881a010b313447b82044d4b2d1875bc_ncud_8byvr_324.jpg';

export { DEFAULT_LOGO, IMAGE_CONTAINER };
