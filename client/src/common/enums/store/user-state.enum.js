const userState = {
  UNAUTHORIZED: 'UNAUTHORIZED',
  UNREGISTRATED: 'UNREGISTRATED'
};

export { userState };
