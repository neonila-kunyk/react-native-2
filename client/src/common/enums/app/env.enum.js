const { REACT_APP_SOCKET_SERVER } = process.env;

const ENV = {
  SOCKET_URL: REACT_APP_SOCKET_SERVER || 'http://192.168.0.108:3002'
};

export { ENV };
