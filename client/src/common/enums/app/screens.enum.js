const Screens = {
  HOME: 'Home',
  AUTH: 'Auth',
  POSTS: 'Posts',
  SIGN_UP: 'SignUp',
  COMMENTS: 'Comments',
  CREATE: 'Create',
  PROFILE: 'Profile',
  EXIT: 'Exit'
};

export { Screens };
