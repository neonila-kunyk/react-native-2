const Colors = {
  BLUE: '#2185d0',
  GREEN: '#00b5ad',
  BLACK: '#111',
  DARCK_GREY: '#666',
  LIGHT_GREY: '#ccc',
  MORE_LIGHT_GREY: '#eee',
  MOST_LIGHT_GREY: '#f6f6f6',
  WHITE: '#fff',
  DARCK_RED: '#ff3a3a',
  LIGHT_RED: '#ffcaca'
};

export { Colors };
