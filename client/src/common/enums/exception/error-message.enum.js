const ErrorMessage = {
  UNAUTHORIZED: 'Please authorize before',
  INTERNAL_SERVER_ERROR: 'Server error. Please try again later',
  DEFAULT: 'Unexpected error'
};

export { ErrorMessage };
