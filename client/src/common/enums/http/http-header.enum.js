const HttpHeader = {
  CONTENT_TYPE: 'Content-Type',
  AUTHORIZATION: 'Authorization'
};

export { HttpHeader };
