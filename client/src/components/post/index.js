import React from 'react';
import { Text, View, Pressable, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import { Colors } from '../../common/enums';
import { toDayJS } from '../../helpers';
import common from '../styles';
import styles from './styles';

const Post = ({ post, onPostLike, sharePost, openComments, isLiked, style }) => (
  <View style={[common.container, styles.container, style]}>
    {
      post.image?.link && (
        <Image source={{ uri: post.image.link }} style={styles.image} />
      )
    }
    <View style={styles.body}>
      <Text style={styles.info}>{`posted by ${post.user.username} - ${toDayJS(post.createdAt).fromNow()}`}</Text>
      <Text style={styles.text}>{ post.body }</Text>
    </View>
    <View style={styles.buttons}>
      <Pressable style={styles.button} onPress={() => onPostLike(post.id)}>
        <Icon name="thumbs-up" style={[styles.icons, isLiked ? { color: Colors.GREEN } : null]} />
      </Pressable>
      <Text style={styles.buttonLabel}>{ post.likeCount }</Text>
      <Pressable style={styles.button}>
        <Icon name="thumbs-down" style={styles.icons} />
      </Pressable>
      <Text style={styles.buttonLabel}>{ post.dislikeCount }</Text>
      <Pressable style={styles.button} onPress={() => openComments(post.id)}>
        <Icon name="comment" style={styles.icons} />
      </Pressable>
      <Text style={styles.buttonLabel}>{ post.commentCount }</Text>
      <Pressable style={styles.button} onPress={() => sharePost(post.id)}>
        <Icon name="share-alt" style={styles.icons} />
      </Pressable>
    </View>
  </View>
);

Post.defaultProps = {
  onPostLike: () => {},
  sharePost: () => {},
  openComments: () => {}
};

const postObj = {
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  commentCount: PropTypes.string,
  createdAt: PropTypes.string,
  dislikeCount: PropTypes.string,
  image: PropTypes.shape({
    id: PropTypes.string,
    link: PropTypes.string
  }),
  imageId: PropTypes.string,
  likeCount: PropTypes.string,
  updatedAt: PropTypes.string,
  user: PropTypes.shape({
    id: PropTypes.string,
    username: PropTypes.string,
    image: PropTypes.shape({
      id: PropTypes.string,
      link: PropTypes.string
    })
  }),
  userId: PropTypes.string.isRequired
};

Post.propTypes = {
  post: PropTypes.shape(postObj).isRequired,
  onPostLike: PropTypes.func,
  sharePost: PropTypes.func,
  openComments: PropTypes.func,
  isLiked: PropTypes.bool.isRequired
};

export { Post };
