import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    width: undefined,
    marginTop: 0,
    padding: 0,
    marginVertical: '0.5rem',
    marginHorizontal: '1.5rem',
    justifyContent: 'center'
  },
  image: {
    width: '100%',
    height: '15rem',
    resizeMode: 'cover'
  },
  info: {
    fontSize: '1rem',
    color: Colors.DARCK_GREY,
    marginHorizontal: '1rem',
    marginBottom: '0.2rem'
  },
  body: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: Colors.MORE_LIGHT_GREY,
    paddingVertical: '0.75rem'
  },
  buttons: {
    width: '100%',
    marginVertical: '0.75rem',
    marginHorizontal: '1rem',
    flexDirection: 'row'
  },
  text: {
    flex: 1,
    fontSize: '1rem',
    marginHorizontal: '1rem'
  },
  icons: {
    width: 20,
    fontSize: '1.2rem',
    textAlign: 'center',
    color: Colors.BLACK,
    textAlignVertical: 'center'
  },
  button: {
    flexDirection: 'row',
    marginRight: '0.4rem',
    justifyContent: 'center'
  },
  buttonLabel: {
    fontSize: '1.1rem',
    fontWeight: '600',
    color: Colors.BLACK,
    marginRight: '1rem'
  }
});
