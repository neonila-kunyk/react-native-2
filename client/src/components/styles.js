import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../common/enums';

export default EStyleSheet.create({
  container: {
    width: '100%',
    marginTop: '1.5rem',
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    padding: '0.75rem',
    backgroundColor: Colors.WHITE
  },
  input: {
    height: '20rem',
    width: '100%',
    fontSize: '1rem',
    color: Colors.BLACK,
    borderWidth: 1,
    borderColor: Colors.LIGHT_GREY,
    borderRadius: 5,
    padding: '1rem',
    textAlignVertical: 'top'
  },
  icons: {
    width: 30,
    height: '2rem',
    fontSize: '1.2rem',
    textAlign: 'center',
    color: Colors.WHITE,
    textAlignVertical: 'center',
    marginRight: '1rem'
  },
  image: {
    width: '18rem',
    height: '15rem',
    resizeMode: 'contain',
    marginBottom: '0.75rem'
  },
  button: {
    flexDirection: 'row',
    width: '100%',
    height: '2.5rem',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: '1rem',
    fontWeight: 'bold',
    color: Colors.WHITE
  }
});

export const getStyleByHeight = height => EStyleSheet.create({ component: { height } }).component;
