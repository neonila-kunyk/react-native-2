import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  button: {
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    marginRight: '0.5rem',
    height: '2rem',
    fontSize: '2rem',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: Colors.BLACK
  }
});
