import React from 'react';
import { Text, View } from 'react-native';
import Image from 'react-native-scalable-image';
import PropTypes from 'prop-types';
import { toDayJS, scale } from '../../helpers';
import styles from './styles';
import { DEFAULT_USER_AVATAR } from '../../common/constants';

const Comment = ({ comment, style }) => (
  <View style={[styles.container, style]}>
    <Image
      width={scale(50)}
      source={{ uri: comment.user?.image?.link || DEFAULT_USER_AVATAR }}
      style={styles.image}
    />
    <View style={styles.body}>
      <View style={styles.info}>
        <Text style={styles.username}>{comment.user.username}</Text>
        <Text style={styles.date}>{toDayJS(comment.createdAt).fromNow()}</Text>
      </View>
      <Text style={styles.text}>{comment.body}</Text>
    </View>
  </View>
);

const commentObj = {
  body: PropTypes.string.isRequired,
  createdAt: PropTypes.string,
  id: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  updatedAt: PropTypes.string,
  user: PropTypes.shape({
    id: PropTypes.string,
    image: PropTypes.shape({
      id: PropTypes.string,
      link: PropTypes.string
    }),
    username: PropTypes.string
  }),
  userId: PropTypes.string.isRequired
};

Comment.propTypes = {
  comment: PropTypes.shape(commentObj).isRequired
};

export { Comment };
