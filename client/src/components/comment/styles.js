import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    marginVertical: '0.75rem',
    marginHorizontal: '0.75rem',
    backgroundColor: Colors.WHITE,
    justifyContent: 'center'
  },
  image: {
    resizeMode: 'contain',
    borderRadius: 5
  },
  info: {
    flexDirection: 'row',
    marginHorizontal: '1rem',
    marginBottom: '0.2rem',
    alignItems: 'flex-start'
  },
  username: {
    fontSize: '1rem',
    lineHeight: '1rem',
    fontWeight: '700',
    color: Colors.BLACK
  },
  date: {
    fontSize: '0.85rem',
    lineHeight: '1rem',
    color: Colors.DARCK_GREY,
    marginLeft: '0.5rem'
  },
  body: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  text: {
    flex: 1,
    fontSize: '1rem',
    marginHorizontal: '1rem'
  }
});
