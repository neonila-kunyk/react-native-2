import React from 'react';
import { View, Pressable } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ToggleSwitch from 'toggle-switch-react-native';
import PropTypes from 'prop-types';
import { Filter, Colors } from '../../common/enums';
import styles from './styles';

const ToggleFilter = ({ firstSwitch, secondSwitch, onOff, filter, setFilter, isFilterVisible, setIsFilterVisible, style }) => {
  const filterInfo = type => {
    if (filter === type) {
      setFilter(Filter.NONE);
      onOff();
    } else {
      setFilter(type);
      if (type === Filter.FIRST) {
        firstSwitch.onToggle();
      } else {
        secondSwitch.onToggle();
      }
    }
  };

  return (
    <View style={[styles.container, style]}>
      {isFilterVisible && (
      <View style={styles.filters}>
        <ToggleSwitch
          style={styles.toggle}
          isOn={filter === Filter.FIRST}
          onColor={Colors.BLUE}
          offColor={Colors.LIGHT_GREY}
          label={firstSwitch.label}
          labelStyle={styles.toggleLabel}
          onToggle={() => filterInfo(Filter.FIRST)}
        />
        <ToggleSwitch
          style={styles.toggle}
          isOn={filter === Filter.SECOND}
          onColor={Colors.BLUE}
          offColor={Colors.LIGHT_GREY}
          label={secondSwitch.label}
          labelStyle={styles.toggleLabel}
          onToggle={() => filterInfo(Filter.SECOND)}
        />
      </View>
      )}
      {setIsFilterVisible && (
      <Pressable style={styles.button} onPress={() => setIsFilterVisible(prev => !prev)}>
        <Icon name="filter" style={styles.icons} />
      </Pressable>
      )}
    </View>
  );
};

ToggleFilter.defaultProps = {
  isFilterVisible: true,
  setIsFilterVisible: null
};

const switchObj = {
  label: PropTypes.string.isRequired,
  onToggle: PropTypes.func.isRequired
};

ToggleFilter.propTypes = {
  firstSwitch: PropTypes.shape(switchObj).isRequired,
  secondSwitch: PropTypes.shape(switchObj).isRequired,
  onOff: PropTypes.func.isRequired,
  filter: PropTypes.string.isRequired,
  setFilter: PropTypes.func.isRequired,
  isFilterVisible: PropTypes.bool,
  setIsFilterVisible: PropTypes.func
};

export { ToggleFilter };
