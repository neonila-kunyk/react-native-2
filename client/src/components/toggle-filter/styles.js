import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    marginHorizontal: '1.5rem',
    flexDirection: 'row'
  },
  toggle: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-end',
    marginVertical: '0.4rem'
  },
  toggleLabel: {
    fontSize: '1.2rem',
    color: Colors.BLACK
  },
  icons: {
    fontSize: '1.5rem',
    color: Colors.BLACK
  },
  button: {
    flexDirection: 'row',
    marginLeft: 'auto'
  }
});
