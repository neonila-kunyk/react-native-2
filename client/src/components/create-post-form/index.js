import React, { useState, useEffect } from 'react';
import { Text, Pressable, View, TextInput, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as ImagePicker from 'expo-image-picker';
import PropTypes from 'prop-types';
import { Colors } from '../../common/enums';
import { setKeyboardHandlers } from '../../helpers';
import common, { getStyleByHeight } from '../styles';
import { IMAGE_CONTAINER } from '../../common/constants';
import styles from './styles';

const CreatePostForm = ({ firstButton, secondButton, numberOfRows, placeholder, height, autoFocus, style }) => {
  const [postText, setPostText] = useState('');
  const [image, setImage] = useState(null);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    if (firstButton) {
      const onHideKeyboard = () => setKeyboardVisible(false);
      const onShowKeyboard = () => setKeyboardVisible(true);
      setKeyboardHandlers({ onHideKeyboard, onShowKeyboard });
    }
  }, []);

  const onPressOnFirstButton = async () => {
    const result = await ImagePicker.launchImageLibraryAsync();
    if (!result.cancelled) {
      try {
        const { uri } = result;
        const img = await firstButton.onPress(uri);
        setImage({ uri: img.link, id: img.id });
      } catch {
        Alert.alert('Sorry, we can\'t upload your image');
      }
    }
  };

  const onPressOnSecondButton = async () => {
    if (!postText) {
      return;
    }
    await secondButton.onPress({ imageId: image?.id, body: postText });
    setPostText('');
    setImage(null);
  };

  return (
    <View style={[common.container, styles.container, style]}>
      {!isKeyboardVisible && firstButton
        && (
        <Image style={common.image} source={{ uri: image?.uri || IMAGE_CONTAINER }} />
        )}
      <TextInput
        style={[common.input, height ? getStyleByHeight(height) : null]}
        underlineColorAndroid="transparent"
        onChangeText={text => { setPostText(text); }}
        value={postText}
        placeholder={placeholder}
        placeholderTextColor={Colors.LIGHT_GREY}
        numberOfLines={numberOfRows}
        multiline
        autoFocus={autoFocus}
      />
      {firstButton && (
      <Pressable style={[common.button, styles.button, { backgroundColor: Colors.GREEN }]} onPress={onPressOnFirstButton}>
        {firstButton.icon && (
        <Icon name={firstButton.icon} style={common.icons} />
        )}
        <Text style={common.buttonText}>{firstButton.text}</Text>
      </Pressable>
      )}
      <Pressable style={[common.button, styles.button]} onPress={onPressOnSecondButton}>
        {secondButton.icon && (
        <Icon name={secondButton.icon} style={common.icons} />
        )}
        <Text style={common.buttonText}>{secondButton.text}</Text>
      </Pressable>
    </View>
  );
};

CreatePostForm.defaultProps = {
  firstButton: null,
  numberOfRows: 15,
  placeholder: '',
  height: '',
  autoFocus: false
};

const buttonObj = {
  icon: PropTypes.string,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
};

CreatePostForm.propTypes = {
  firstButton: PropTypes.shape(buttonObj),
  secondButton: PropTypes.shape(buttonObj).isRequired,
  numberOfRows: PropTypes.number,
  placeholder: PropTypes.string,
  height: PropTypes.string,
  autoFocus: PropTypes.bool
};

export { CreatePostForm };
