import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    padding: '0.75rem',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  button: {
    backgroundColor: Colors.BLUE,
    marginTop: '1rem'
  }
});

export const getStyleByHeight = height => EStyleSheet.create({ component: { height } }).component;
