import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: Colors.LIGHT_GREY,
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: '1.25rem',
    backgroundColor: Colors.WHITE
  },
  icons: {
    color: Colors.DARCK_GREY,
    marginRight: 0
  },
  input: {
    flex: 1,
    height: '2.5rem',
    fontSize: '1rem',
    color: Colors.DARCK_GREY,
    borderWidth: 0,
    paddingHorizontal: 0
  }
});
