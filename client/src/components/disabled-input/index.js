import React from 'react';
import { TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import common from '../styles';
import styles from './styles';

const DisabledInput = ({ value, icon, style }) => (
  <View style={[styles.container, style]}>
    <Icon name={icon} style={[common.icons, styles.icons]} />
    <TextInput
      style={styles.input}
      value={value}
      editable={false}
    />
  </View>
);

DisabledInput.propTypes = {
  value: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

export { DisabledInput };
