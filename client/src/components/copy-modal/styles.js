import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  root: {
    width: '100%',
    marginTop: '1.5rem',
    padding: '1rem',
    backgroundColor: Colors.WHITE,
    justifyContent: 'center'
  },
  header: {
    fontSize: '1.75rem',
    marginBottom: '0.25rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  title: {
    fontSize: '1.75rem',
    color: Colors.BLACK
  },
  copied: {
    fontSize: '1.25rem',
    color: Colors.GREEN
  },
  body: {
    width: '100%',
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: Colors.LIGHT_GREY,
    paddingTop: '0.75rem'
  },
  text: {
    flex: 1,
    fontSize: '1rem'
  },
  copyIcon: {
    marginRight: 0,
    marginLeft: '0.2rem'
  },
  button: {
    width: '5rem',
    justifyContent: 'flex-end',
    backgroundColor: Colors.GREEN
  }
});
