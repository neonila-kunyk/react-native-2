import React, { useState } from 'react';
import { Text, View, Pressable, Clipboard, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import common from '../styles';
import styles from './styles';

const CopyModal = ({ title, text, isModalVisible, hide, style }) => {
  const [isCopied, setIsCopied] = useState(false);

  const onCopy = () => {
    if (text) {
      Clipboard.setString(text);
      setIsCopied(true);
    } else {
      Alert.alert('No information to copy');
    }
  };
  return (
    <Modal
      isVisible={isModalVisible}
      onBackdropPress={hide}
      onBackButtonPress={hide}
      style={[styles.container, style]}
    >
      <View style={styles.root}>
        <View style={styles.header}>
          <Text style={styles.title}>{title}</Text>
          {isCopied && <Text style={styles.copied}>copied</Text>}
        </View>
        <View style={styles.body}>
          <Text style={styles.text}>{text}</Text>
          <Pressable style={[common.button, styles.button]} onPress={onCopy}>
            <Text style={common.buttonText}>Copy</Text>
            <Icon name="clipboard" style={[common.icons, styles.copyIcon]} />
          </Pressable>
        </View>
      </View>
    </Modal>
  );
};

CopyModal.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isModalVisible: PropTypes.bool.isRequired,
  hide: PropTypes.func.isRequired
};

export { CopyModal };
