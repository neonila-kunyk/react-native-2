import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import common from '../styles';
import styles from './styles';

const FramedText = ({ label, link, navigate, style }) => (
  <View style={[common.container, styles.container, style]}>
    <Text style={styles.label}>{label}</Text>
    <Text style={styles.link} onPress={() => navigate()}>{link}</Text>
  </View>
);

FramedText.defaultProps = {
  label: '',
  link: '',
  navigate: () => {}
};

FramedText.propTypes = {
  label: PropTypes.string,
  link: PropTypes.string,
  navigate: PropTypes.func
};

export { FramedText };
