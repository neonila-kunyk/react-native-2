import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  label: {
    fontSize: '1rem'
  },
  link: {
    fontSize: '1rem',
    color: Colors.BLUE,
    marginLeft: '0.5rem'
  }
});
