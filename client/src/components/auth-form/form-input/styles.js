import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../../common/enums';

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: Colors.DARCK_GREY,
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: '1.25rem'
  },
  icons: {
    width: 30,
    height: '2rem',
    fontSize: '1.2rem',
    textAlign: 'center',
    color: Colors.DARCK_GREY,
    textAlignVertical: 'center'
  },
  input: {
    flex: 1,
    height: '2.5rem',
    fontSize: '1rem',
    color: Colors.BLACK,
    borderWidth: 0,
    paddingHorizontal: 0
  },
  focusedContainer: {
    borderColor: Colors.BLUE
  },
  focusedIcons: {
    color: Colors.BLACK
  },
  invalidContainer: {
    borderColor: Colors.DARCK_RED,
    backgroundColor: Colors.LIGHT_RED
  },
  invalidIcons: {
    color: Colors.DARCK_RED
  }
});
