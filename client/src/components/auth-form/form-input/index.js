import React, { useState } from 'react';
import { TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import { toUpperCaseFirst } from '../../../helpers';
import { Input, Colors } from '../../../common/enums';
import styles from './styles';

const FormInput = ({ onChangeText, onSubmitEditing, onFocus, onBlur,
  value, icon, keyboardType, placeholder, isFocused, isEmpty }) => {
  const [visible, setVisibility] = useState(false);
  const eye = !visible ? 'eye-slash' : 'eye';
  const isPassword = toUpperCaseFirst(Input.PASSWORD.toLowerCase()) === placeholder;

  const getStyle = className => {
    if (isFocused) {
      return [styles[className], styles[`focused${toUpperCaseFirst(className)}`]];
    }
    if (isEmpty) {
      return [styles[className], styles[`invalid${toUpperCaseFirst(className)}`]];
    }
    return styles[className];
  };

  return (
    <View style={getStyle('container')}>
      <Icon name={icon} style={getStyle('icons')} />
      <TextInput
        style={getStyle('input')}
        underlineColorAndroid="transparent"
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        onFocus={onFocus}
        onBlur={onBlur}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={isEmpty && !isFocused ? Colors.DARCK_RED : Colors.LIGHT_GREY}
        keyboardType={keyboardType}
        secureTextEntry={isPassword ? !visible : false}
      />
      {isPassword && (
        <Icon
          name={eye}
          onPress={() => setVisibility(!visible)}
          style={getStyle('icons')}
        />
      )}
    </View>
  );
};

FormInput.defaultProps = {
  onChangeText: () => { },
  onFocus: () => { },
  onSubmitEditing: () => { },
  onBlur: () => { },
  keyboardType: 'default',
  placeholder: '',
  isFocused: false,
  isEmpty: false
};

FormInput.propTypes = {
  value: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onChangeText: PropTypes.func,
  onFocus: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  onBlur: PropTypes.func,
  keyboardType: PropTypes.string,
  placeholder: PropTypes.string,
  isFocused: PropTypes.bool,
  isEmpty: PropTypes.bool
};

export { FormInput };
