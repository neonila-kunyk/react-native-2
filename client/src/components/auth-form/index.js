import React, { useState } from 'react';
import { Text, Pressable, View } from 'react-native';
import PropTypes from 'prop-types';
import { FormInput } from './form-input';
import { Input } from '../../common/enums';
import common from '../styles';
import styles from './styles';

const AuthForm = ({ isSignUp, buttonText, onConfirm, style }) => {
  const [user, setUser] = useState({
    email: '',
    password: '',
    name: ''
  });
  const [focusedInput, setFocusedInput] = useState('');
  const [isBeforeInputEmail, setIsBeforeInputEmail] = useState(true);
  const [isBeforeInputName, setIsBeforeInputName] = useState(true);
  const [isBeforeInputPassword, setIsBeforeInputPassword] = useState(true);

  const onPress = () => (isSignUp ? onConfirm(user.email, user.name, user.password) : onConfirm(user.email, user.password));

  return (
    <View style={[common.container, style]}>
      <FormInput
        isFocused={focusedInput === Input.EMAIL}
        isEmpty={!isBeforeInputEmail && user.email === ''}
        onChangeText={text => setUser(prev => ({ ...prev, email: text }))}
        onFocus={() => setFocusedInput(Input.EMAIL)}
        onSubmitEditing={() => focusedInput === Input.EMAIL && setFocusedInput('')}
        onBlur={() => setIsBeforeInputEmail(false)}
        value={user.email}
        icon="at"
        keyboardType="email-address"
        placeholder="Email"
      />
      {isSignUp && (
        <FormInput
          isFocused={focusedInput === Input.NAME}
          isEmpty={!isBeforeInputName && user.name === ''}
          onChangeText={text => setUser(prev => ({ ...prev, name: text }))}
          onFocus={() => setFocusedInput(Input.NAME)}
          onSubmitEditing={() => focusedInput === Input.NAME && setFocusedInput('')}
          onBlur={() => setIsBeforeInputName(false)}
          value={user.name}
          icon="user"
          placeholder="Username"
        />
      )}
      <FormInput
        isFocused={focusedInput === Input.PASSWORD}
        isEmpty={!isBeforeInputPassword && user.password === ''}
        onChangeText={text => setUser(prev => ({ ...prev, password: text }))}
        onFocus={() => setFocusedInput(Input.PASSWORD)}
        onSubmitEditing={() => focusedInput === Input.PASSWORD && setFocusedInput('')}
        onBlur={() => setIsBeforeInputPassword(false)}
        value={user.password}
        icon="lock"
        placeholder="Password"
      />
      <Pressable style={[common.button, styles.button]} onPress={onPress}>
        <Text style={common.buttonText}>{buttonText}</Text>
      </Pressable>
    </View>
  );
};

AuthForm.defaultProps = {
  isSignUp: false
};

AuthForm.propTypes = {
  isSignUp: PropTypes.bool,
  buttonText: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export { AuthForm };
