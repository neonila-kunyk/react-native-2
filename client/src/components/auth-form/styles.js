import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  button: {
    backgroundColor: Colors.BLUE
  }
});
