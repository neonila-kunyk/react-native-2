import React from 'react';
import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { DEFAULT_LOGO } from '../../common/constants';

const CompanyLogo = ({ logoUri, name, style }) => (
  <View style={[styles.container, style]}>
    <Image
      style={styles.logo}
      source={{ uri: logoUri }}
    />
    <Text style={styles.name}>{name}</Text>
  </View>
);

CompanyLogo.defaultProps = {
  logoUri: DEFAULT_LOGO
};

CompanyLogo.propTypes = {
  logoUri: PropTypes.string,
  name: PropTypes.string.isRequired
};

export { CompanyLogo };
