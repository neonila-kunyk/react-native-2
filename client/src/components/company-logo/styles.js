import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../common/enums';

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: '25%',
    height: '100%',
    borderRadius: 1000,
    resizeMode: 'contain',
    marginRight: '0.75rem'
  },
  name: {
    fontSize: '1.5rem',
    fontWeight: '700',
    color: Colors.DARCK_GREY
  }
});
