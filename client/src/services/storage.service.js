const secureStoreServiceCreator = storage => {
  const getItem = async key => await storage.getItem(key);
  const setItem = async (key, value) => await storage.setItem(key, value);
  const removeItem = async key => await storage.removeItem(key);

  return { getItem, setItem, removeItem };
};

export default secureStoreServiceCreator;
