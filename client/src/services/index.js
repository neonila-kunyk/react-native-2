import AsyncStorage from '@react-native-async-storage/async-storage';
import { senderReq, getOpts } from '../helpers';

import authServiceCreator from './auth.service';
import commentServiceCreatort from './comment.service';
import authServiceCreatore from './image.service';
import postServiceCreator from './post.service';
import storeServiceCreator from './storage.service';

export const storeService = storeServiceCreator(AsyncStorage);

const sendReq = senderReq(storeService);

export const authService = authServiceCreator(sendReq, getOpts);
export const commentService = commentServiceCreatort(sendReq, getOpts);
export const imageService = authServiceCreatore(sendReq, getOpts);
export const postService = postServiceCreator(sendReq, getOpts);

