import { HttpMethod, userState } from '../common/enums';

const authServiceCreator = (sendReq, getOpts) => {
  const logIn = async payload => await sendReq('/api/auth/login', getOpts(HttpMethod.POST, payload, userState.UNAUTHORIZED));
  const signUp = async payload => await sendReq('/api/auth/register', getOpts(HttpMethod.POST, payload, userState.UNAUTHORIZED));
  const getCurrentUser = async () => await sendReq('/api/auth/user', getOpts(HttpMethod.GET));

  return { logIn, signUp, getCurrentUser };
};

export default authServiceCreator;
