import { HttpMethod } from '../common/enums';

const postServiceCreator = (sendReq, getOpts) => {
  const getAllPosts = async filter => await sendReq('/api/posts', getOpts(HttpMethod.GET, filter));
  const getPost = async id => await sendReq(`/api/posts/${id}`, getOpts(HttpMethod.GET));
  const addPost = async payload => await sendReq('/api/posts', getOpts(HttpMethod.POST, payload));
  const editPost = async (id, payload) => await sendReq(`/api/posts/${id}`, getOpts(HttpMethod.PUT, payload));
  const deletePost = async id => await sendReq(`/api/posts/${id}`, getOpts(HttpMethod.DELETE));
  const likePost = async postId => await sendReq('/api/posts/react', getOpts(HttpMethod.PUT, { postId, isLike: true }));
  const getUserReactions = async () => await sendReq('/api/posts/reactions', getOpts(HttpMethod.GET));

  return { getAllPosts, getPost, addPost, editPost, deletePost, likePost, getUserReactions };
};

export default postServiceCreator;
