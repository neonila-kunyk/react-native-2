import mime from 'mime';
import { HttpMethod } from '../common/enums';

const imageServiceCreator = (sendReq, getOpts) => {
  const getFormData = imageUri => {
    const image = {
      uri: imageUri,
      type: mime.getType(imageUri),
      name: imageUri.split('/').pop()
    };
    const formData = new FormData();
    formData.append('image', image, image.name);
    return formData;
  };

  const uploadImage = async imageUri => await sendReq('/api/images', getOpts(HttpMethod.POST, getFormData(imageUri)));

  return { uploadImage };
};

export default imageServiceCreator;
