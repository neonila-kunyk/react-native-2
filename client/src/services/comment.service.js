import { HttpMethod } from '../common/enums';

const commentServiceCreator = (sendReq, getOpts) => {
  const getComment = async id => await sendReq(`/api/comments/${id}`, getOpts(HttpMethod.GET));
  const addComment = async payload => await sendReq('/api/comments', getOpts(HttpMethod.POST, payload));
  const editComment = async (id, payload) => sendReq(`/api/comments/${id}`, getOpts(HttpMethod.PUT, payload));
  const deleteComment = async id => sendReq(`/api/comments/${id}`, getOpts(HttpMethod.DELETE));

  return { getComment, addComment, editComment, deleteComment };
};

export default commentServiceCreator;
