import { HttpCode, ErrorMessage } from '../common/enums';

const getMessage = code => {
  if (code === HttpCode.UNAUTHORIZED) {
    return ErrorMessage.UNAUTHORIZED;
  } if (code === HttpCode.INTERNAL_SERVER_ERROR) {
    return ErrorMessage.INTERNAL_SERVER_ERROR;
  }
  return ErrorMessage.DEFAULT;
};

export { getMessage };
