import { stringify } from 'query-string';
import { ContentType, HttpMethod, userState } from '../common/enums';

const getOpts = (method, payload, hasAuth) => {
  const options = { method, query: '', hasAuth: true };
  if (payload) {
    if (method === HttpMethod.GET) {
      options.query = `?${stringify(payload)}`;
    } else if (!(payload instanceof FormData)) {
      options.contentType = ContentType.JSON;
      options.payload = JSON.stringify(payload);
    } else {
      options.payload = payload;
    }
  }
  if (hasAuth === userState.UNAUTHORIZED) {
    options.hasAuth = false;
  }
  return options;
};

export { getOpts };
