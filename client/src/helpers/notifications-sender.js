import { showMessage } from 'react-native-flash-message';
import { Colors } from '../common/enums';

const sendNotification = text => {
  showMessage({
    message: text,
    type: 'default',
    backgroundColor: Colors.BLUE,
    color: Colors.WHITE
  });
};

export { sendNotification };
