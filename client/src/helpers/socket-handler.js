import io from 'socket.io-client';
import { ENV } from '../common/enums';

const handleSocket = (userId, showNotification, onApplyPost) => {
  const socket = io(ENV.SOCKET_URL);
  if (!userId) {
    return undefined;
  }
  socket.emit('createRoom', userId);
  socket.on('like', async () => {
    showNotification('Your post was liked!');
  });
  socket.on('dislike', async () => {
    showNotification('Your post was disliked!');
  });
  socket.on('new_post', post => {
    if (post.userId !== userId) {
      onApplyPost(post.id);
    }
  });

  return () => {
    socket.close();
  };
};

export { handleSocket };
