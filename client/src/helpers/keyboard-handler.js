import { Keyboard } from 'react-native';

const setShowHandler = onShowKeyboard => {
  const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', onShowKeyboard);
  return () => {
    keyboardDidShowListener.remove();
  };
};

const setHideHandler = onHideKeyboard => {
  const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', onHideKeyboard);
  return () => {
    keyboardDidHideListener.remove();
  };
};

const setKeyboardHandlers = ({ onHideKeyboard, onShowKeyboard }) => {
  if (onShowKeyboard && onHideKeyboard) {
    setHideHandler(onHideKeyboard);
    setShowHandler(onShowKeyboard);
  } else if (onHideKeyboard) {
    setHideHandler(onHideKeyboard);
  } else if (onShowKeyboard) {
    setShowHandler(onShowKeyboard);
  }
};

export { setKeyboardHandlers };
