import { HttpHeader, HttpMethod, StorageKey } from '../common/enums';
import { HttpError } from '../exceptions';
import { BASE_URL } from '../common/constants';

const senderReq = storage => async (url, options = {}) => {
  const { method = HttpMethod.GET, payload = null, hasAuth = true, contentType, query } = options;

  const headers = {};
  if (contentType) {
    headers[HttpHeader.CONTENT_TYPE] = contentType;
  }
  if (hasAuth) {
    headers[HttpHeader.AUTHORIZATION] = `Bearer ${await storage.getItem(StorageKey.TOKEN)}`;
  }
  const fullUrl = `${BASE_URL}${url}${query}`;

  try {
    const response = await fetch(fullUrl, { method, headers, body: payload });
    if (response.ok) {
      return await response.json();
    }
    throw new HttpError({ status: response.status, message: `Network error: status ${response.status}, url: ${fullUrl}` });
  } catch (err) {
    console.error(err.message);
    throw err;
  }
};

export { senderReq };
