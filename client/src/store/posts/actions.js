import { commentService, postService } from '../../services';
import {
  setPosts,
  removePosts,
  addPost,
  editPosts,
  setExpandedPostId,
  setLikedPosts,
  addLikedPost,
  removeLikedPost,
  setComments,
  addComment,
  setErrorMessage
} from './reducer';
import { toDayJS, getMessage } from '../../helpers';

const loadLikedPosts = () => async dispatch => {
  try {
    const likedPosts = (await postService.getUserReactions()).map(reaction => reaction.postId);
    dispatch(setLikedPosts(likedPosts));
  } catch (err) {
    setErrorMessage(getMessage(err.status));
  }
};

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const resetPosts = filter => async dispatch => {
  dispatch(removePosts());
  dispatch(loadPosts(filter));
};

const createPost = post => async dispatch => {
  try {
    const { id } = await postService.addPost(post);
    try {
      const newPost = await postService.getPost(id);
      dispatch(addPost(newPost));
    } catch (err) {
      setErrorMessage(getMessage(err.status));
    }
  } catch (err) {
    setErrorMessage(getMessage(err.status));
  }
};

const applyPost = postId => async dispatch => {
  try {
    const post = await postService.getPost(postId);
    dispatch(addPost(post));
  } catch (err) {
    setErrorMessage(getMessage(err.status));
  }
};

const toggleExpandedPost = postId => async dispatch => {
  if (postId) {
    try {
      const post = await postService.getPost(postId);
      dispatch(setExpandedPostId(postId));
      dispatch(setComments([...post.comments].sort((a, b) => toDayJS(b.createdAt).diff(a.createdAt))));
    } catch (err) {
      setErrorMessage(getMessage(err.status));
    }
  } else {
    dispatch(setExpandedPostId(''));
    dispatch(setComments([]));
  }
};

const likePost = postId => async (dispatch, getRootState) => {
  try {
    const { id } = await postService.likePost(postId);
    const { posts, likedPosts } = getRootState().posts;
    if (likedPosts.includes(postId)) {
      dispatch(removeLikedPost(postId));
    } else {
      dispatch(addLikedPost(postId));
    }
    const getEditedPost = post => ({
      ...post,
      likeCount: String(+post.likeCount + (id ? 1 : -1))
    });
    const postToEdit = posts.find(post => post.id === postId);
    dispatch(editPosts(getEditedPost(postToEdit)));
  } catch (err) {
    setErrorMessage(getMessage(err.status));
  }
};

const createComment = commentData => async (dispatch, getRootState) => {
  try {
    const { id } = await commentService.addComment(commentData);
    try {
      const comment = await commentService.getComment(id);
      const { posts } = getRootState().posts;
      const getEditedPost = post => ({
        ...post,
        commentCount: String(+post.commentCount + 1),
        comments: [...(post.comments || []), comment]
      });
      const postToEdit = posts.filter(post => post.id === comment.postId);
      dispatch(editPosts(getEditedPost(postToEdit)));
      dispatch(addComment(comment));
    } catch (err) {
      setErrorMessage(getMessage(err.status));
    }
  } catch (err) {
    setErrorMessage(getMessage(err.status));
  }
};

export {
  loadLikedPosts,
  loadPosts,
  resetPosts,
  createPost,
  applyPost,
  toggleExpandedPost,
  likePost,
  createComment
};
