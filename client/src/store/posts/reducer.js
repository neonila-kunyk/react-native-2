import { createSlice } from '@reduxjs/toolkit';
import 'react-native-get-random-values';

const contactsSlice = createSlice({
  name: 'posts',
  initialState: {
    posts: [],
    expandedPost: '',
    isAllPosts: false,
    likedPosts: [],
    comments: [],
    errorMessage: ''
  },
  reducers: {
    setPosts(state, action) {
      const loadedPosts = action.payload;
      if (!loadedPosts.length) {
        state.isAllPosts = true;
      } else {
        const filteredPosts = loadedPosts.filter(loadedPost => !state.posts.some(post => post.id === loadedPost.id));
        state.posts = [...state.posts, ...filteredPosts];
      }
    },

    removePosts(state) {
      state.posts = [];
    },

    addPost(state, action) {
      const post = action.payload;
      state.posts = [post, ...state.posts];
    },

    editPosts(state, action) {
      const editedPost = action.payload;
      state.posts = state.posts.map(post => (post.id !== editedPost.id ? post : editedPost));
    },

    setExpandedPostId(state, action) {
      const post = action.payload;
      state.expandedPost = post;
    },

    setLikedPosts(state, action) {
      const likedPosts = action.payload;
      state.likedPosts = likedPosts;
    },

    addLikedPost(state, action) {
      const postId = action.payload;
      state.likedPosts.push(postId);
    },

    removeLikedPost(state, action) {
      const postId = action.payload;
      state.likedPosts = state.likedPosts.filter(id => id !== postId);
    },

    setComments(state, action) {
      const comments = action.payload;
      state.comments = comments;
    },

    addComment(state, action) {
      const comment = action.payload;
      state.comments = [comment, ...state.comments];
    },

    setErrorMessage(state, action) {
      const massage = action.payload;
      state.errorMessage = massage;
    }
  }
});

export const {
  setPosts,
  removePosts,
  addPost,
  editPosts,
  setExpandedPostId,
  setLikedPosts,
  addLikedPost,
  removeLikedPost,
  setComments,
  addComment,
  setErrorMessage
} = contactsSlice.actions;
export default contactsSlice.reducer;
