import { authService, storeService } from '../../services';
import { setUser, setUnregistrated, removeUser } from './reducer';
import { StorageKey } from '../../common/enums';

const logIn = userData => async dispatch => {
  try {
    const { user, token } = await authService.logIn(userData);
    dispatch(setUser({ ...user, name: user.username }));
    await storeService.setItem(StorageKey.TOKEN, token);
  } catch {
    dispatch(setUnregistrated());
  }
};

const signUp = userData => async dispatch => {
  try {
    const { user, token } = await authService.signUp({ ...userData, username: userData.name });
    dispatch(setUser({ ...user, name: user.username }));
    await storeService.setItem(StorageKey.TOKEN, token);
  } catch {
    dispatch(removeUser());
  }
};

const logOut = () => dispatch => {
  storeService.removeItem(StorageKey.TOKEN);
  dispatch(removeUser());
};

const setCurrentUser = () => async dispatch => {
  try {
    const user = await authService.getCurrentUser();
    dispatch(setUser({ ...user, name: user.username }));
  } catch (err) {
    dispatch(removeUser());
  }
};

export { logIn, signUp, logOut, setCurrentUser };
