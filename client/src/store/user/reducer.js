/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';
import { userState } from '../../common/enums';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    id: userState.UNAUTHORIZED,
    name: '',
    email: ''
  },
  reducers: {
    setUser(state, action) {
      const { id, email, name, image } = action.payload;
      state.id = id;
      state.email = email;
      state.name = name;
      state.image = image;
    },

    addLikedPost(state, action) {
      const postIs = action.payload;
      state.likedPosts.push(postIs);
    },

    setUnregistrated(state) {
      state.id = userState.UNREGISTRATED;
    },

    removeUser(state) {
      state.id = userState.UNAUTHORIZED;
    }
  }
});

export const { setUser, addLikedPost, setUnregistrated, removeUser } = userSlice.actions;
export default userSlice.reducer;
