import { configureStore } from '@reduxjs/toolkit';
import userReducer from './user/reducer';
import postsReducer from './posts/reducer';

export const store = configureStore({
  reducer: { user: userReducer, posts: postsReducer }
});
