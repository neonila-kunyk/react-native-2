import { createStackNavigator } from 'react-navigation-stack';
import { PostsView, CommentsView } from '../views';

const ContentNavigator = createStackNavigator(
  {
    Posts: {
      screen: PostsView,
      navigationOptions: {
        headerShown: false
      }
    },
    Comments: {
      screen: CommentsView,
      navigationOptions: {
        headerShown: false
      }
    }
  },
  {
    initialRouteName: 'Posts'
  }
);

export default ContentNavigator;
