import { createSwitchNavigator } from 'react-navigation';
import AuthNavigator from './auth.navigator';
import TabNavigator from './tab.navigator';

const AppNavigator = createSwitchNavigator(
  {
    Home: TabNavigator,
    Auth: AuthNavigator
  },
  {
    initialRouteName: 'Auth'
  }
);

export default AppNavigator;
