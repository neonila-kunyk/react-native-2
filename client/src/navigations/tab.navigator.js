import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import ContentNavigator from './content.navigator';
import { ProfileView, CreateView, EmptyView } from '../views';
import { store } from '../store/store';
import { logOut } from '../store/user/actions';
import { Screens } from '../common/enums';

const TabNavigator = createBottomTabNavigator(
  {
    Posts: {
      screen: ContentNavigator,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (<Icon name="home" color={focused ? '#2185d0' : '#999'} size={28} />)
      }
    },
    Create: {
      screen: CreateView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (<Icon name="plus-square" color={focused ? '#2185d0' : '#999'} size={28} />)
      }
    },
    Profile: {
      screen: ProfileView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (<Icon name="user" color={focused ? '#2185d0' : '#999'} size={28} />)
      }
    },
    Exit: {
      screen: EmptyView,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (<Icon name="sign-out" color={focused ? '#2185d0' : '#999'} size={28} />),
        tabBarOnPress: ({ navigation }) => {
          store.dispatch(logOut());
          navigation.navigate(Screens.AUTH);
        }
      }
    }
  },
  {
    tabBarOptions: {
      keyboardHidesTabBar: true,
      showIcon: true,
      showLabel: false
    }
  }
);

export default TabNavigator;
