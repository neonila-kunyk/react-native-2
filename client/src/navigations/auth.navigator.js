import { createStackNavigator } from 'react-navigation-stack';
import { LogInView, SignUpView } from '../views';

const AuthNavigator = createStackNavigator(
  {
    Posts: {
      screen: LogInView,
      navigationOptions: {
        headerShown: false
      }
    },
    SignUp: {
      screen: SignUpView,
      navigationOptions: {
        headerShown: false
      }
    }
  },
  {
    initialRouteName: 'Posts'
  }
);

export default AuthNavigator;
