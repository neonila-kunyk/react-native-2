import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import AppNavigator from './src/navigations';
import { applyPost, setCurrentUser } from './src/store/actions';
import { storeService } from './src/services';
import { StorageKey, userState } from './src/common/enums';
import { sendNotification, handleSocket } from './src/helpers';

const Thread = () => {
  const dispatch = useDispatch();
  const { id: userId } = useSelector(state => state.user);

  useEffect(() => {
    (async () => {
      const token = await storeService.getItem(StorageKey.TOKEN);
      if (token) {
        dispatch(setCurrentUser());
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      if (userId !== userState.UNAUTHORIZED && userId !== userState.UNREGISTRATED) {
        const onApplyPost = postId => dispatch(applyPost(postId));
        handleSocket(userId, sendNotification, onApplyPost);
      }
    })();
  }, [userId]);

  return (
    <AppNavigator />
  );
};

EStyleSheet.build();

export default Thread;
