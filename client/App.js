import React from 'react';
import { Provider } from 'react-redux';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import FlashMessage from 'react-native-flash-message';
import { store } from './src/store/store';
import Thread from './Thread';

const App = () => (
  <Provider store={store}>
    <View style={{ flex: 1 }}>
      <Thread />
      <FlashMessage position="top" />
    </View>
  </Provider>
);

EStyleSheet.build();

export default App;
